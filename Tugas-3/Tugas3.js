//SOAL NO.1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log('SOAL NO.1');
console.log(kataPertama+" "+kataKedua+" "+kataKetiga+" "+kataKeempat)
console.log('\n')

//SOAL NO.2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
var jumlah = parseInt(kataPertama)+parseInt(kataKedua)+parseInt(kataKetiga)+parseInt(kataKeempat)

console.log('SOAL NO.2');
console.log("jumlah: "+jumlah)
console.log('\n')

//SOAL NO.3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24);  
var kataKelima = kalimat.substring(25, 31); 

console.log('SOAL NO.3');
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log('\n')

//NO.4
var nilai=50;

console.log('SOAL NO.4');

if(nilai>=80) {
    console.log("nilai: A");
} else if(nilai>=70) {
    console.log("nilai: B");
} else if(nilai>=60) {
    console.log("nilai: C");
} else if(nilai>=50) {
    console.log("nilai: D");
} else {
    console.log("nilai: E");
}

console.log('\n')

//NO.5
var hari = 8; 
var bulan = 12; 
var tahun = 1996;

console.log('SOAL NO.5')
switch(bulan) {
    case 1:   { console.log(hari+" Januari "+tahun); break; }
    case 2:   { console.log(hari+" Februari "+tahun); break;  }
    case 3:   { console.log(hari+" Maret "+tahun); break;  }
    case 4:   { console.log(hari+" April "+tahun); break;  }
    case 5:   { console.log(hari+" Mei "+tahun); break; }
    case 6:   { console.log(hari+" Juni "+tahun); break;  }
    case 7:   { console.log(hari+" Juli "+tahun); break;  }
    case 8:   { console.log(hari+" Agustus "+tahun); break;  }
    case 9:   { console.log(hari+" September "+tahun); break; }
    case 10:   { console.log(hari+" Oktober "+tahun); break;  }
    case 11:   { console.log(hari+" November "+tahun); break;  }
    case 12:   { console.log(hari+" Desember "+tahun); break;  }}



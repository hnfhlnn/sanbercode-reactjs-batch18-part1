//NO.1 Looping (While)
console.log("------------ NOMOR.1 ------------")
console.log("LOOPING PERTAMA")
var angka=2;
while(angka<=20) {
    console.log(angka+" - I love coding")
    angka+=2;
}

console.log("\n")
console.log("LOOPING KEDUA")
var angka=20;
while(angka>=2) {
    console.log(angka+" - I will become a mobile developer")
    angka-=2;
}

//NO.2 Looping (For)
console.log("\n")
console.log("------------ NOMOR.2 ------------")
for(i=1;i<=20;i++) {
    if(i%2!==0) {
        if(i%3===0) {
            console.log(i+" - I Love Coding")
        } else {
            console.log(i+" - Santai")
        }
    } else {
        console.log(i+" - Berkualitas")
    }
}


//NO.3 Membuat Tangga
console.log("\n")
console.log("------------ NOMOR.3 ------------")
//menentukan panjang tangga
var tangga=7;
var gabung=""
for(i=1;i<=tangga;i++) {
    gabung=gabung+"#";
    console.log(gabung)
}

//NO.4 split kalimat
console.log("\n")
console.log("------------ NOMOR.4 ------------")
var kalimat="saya sangat senang belajar javascript"
var kata = kalimat.split(" ")
console.log(kata)

//NO.5 sort array
console.log("\n")
console.log("------------ NOMOR.5 ------------")
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
console.log(daftarBuah)
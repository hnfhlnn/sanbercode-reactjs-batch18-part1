//No.1
console.log("--------------SOAL NO.1--------------")
function halo() {
    return "Halo Sanbers!"
}

console.log(halo())

//No.2
console.log("\n")
console.log("--------------SOAL NO.2--------------")
function kalikan(angka1,angka2) {
    return angka1*angka2
}

var num1=12
var num2=4
var hasilKali=kalikan(num1,num2)
console.log(hasilKali)

//No.3
console.log("\n")
console.log("--------------SOAL NO.3--------------")
function introduce(nama,umur,alamat,hobi) {
    return "Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!"
}

var name = "hanifah Lainun"
var age = 23
var address = "Jln. Haur Pancuh No.180, Kota Bandung"
var hobby = "Tidur"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

//No.4
console.log("\n")
console.log("--------------SOAL NO.4--------------")

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var objDaftarPeserta = {
    name : arrayDaftarPeserta[0],
    gender: arrayDaftarPeserta[1],
    hobby: arrayDaftarPeserta[2],
    birthYear: arrayDaftarPeserta[3],
}
 
console.log(arrayDaftarPeserta)
console.log(objDaftarPeserta)

//No.5
console.log("\n")
console.log("--------------SOAL NO.5--------------")

var buah = [
    {nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000},
    {nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000},
    {nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000},
    {nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000}
]

console.log(buah[0])

//No.6
console.log("\n")
console.log("--------------SOAL NO.6--------------")

var dataFilm = []
function film(name, duration, genre, year) {
    dataFilm.push({
        judul: name,
        durasi: duration,
        genre: genre,
        tahun: year
    })
}

film("aaa","1 jam", "horror", 1999)
film("bbb","1 jam 30 Menit", "Komedi", 2020)
console.log(dataFilm)



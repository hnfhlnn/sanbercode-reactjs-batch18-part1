console.log("=======================No.1=======================")
const luasLingkaran = ()=>{
    let r=10;
    return console.log("Luas Lingkaran: "+3.14*r*r);
}
const kelilingLingkaran = ()=>{
    let r=10;
    return console.log("Keliling Lingkaran: "+3.14*2*r);
}

luasLingkaran();
kelilingLingkaran();

console.log("\n")
console.log("=======================No.2=======================")
const tambahKata = (string1, string2)=>{
return `${string1} ${string2}`
}

let kalimat =""
kalimat = tambahKata(kalimat,"saya")
kalimat = tambahKata(kalimat,"adalah")
kalimat = tambahKata(kalimat,"seorang")
kalimat = tambahKata(kalimat,"frontend")
kalimat = tambahKata(kalimat,"developer")

console.log(kalimat)

console.log("\n")
console.log("=======================No.3=======================")
const newFunction = (firstName, lastName)=>{
    return {
        firstName,
        lastName,
        fullName() {
        return console.log(`${firstName} ${lastName}`)
        }
    }
    }

newFunction("William", "Imoh").fullName() 

console.log("\n")
console.log("=======================No.4=======================")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation);

console.log("\n")
console.log("=======================No.5=======================")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)



    


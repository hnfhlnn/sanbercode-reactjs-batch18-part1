console.log("=========================No.1=========================")
class Animal {
  constructor(nama) {
    this.name = nama;
    this.legs = 4;
    this.cold_blooded=false;
  }
  get aname() {
    return this.name;
  }
  set aname(x) {
    this.name=x;
  }
}

var sheep = new Animal("shaun");
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

console.log("-----------------------------------------------------")
class Ape extends Animal {
  constructor(nama) {
    super(nama);
    this.legs=2;
  }
  yell() {
    return "Auooo";
  }
}

var sungokong = new Ape("kera sakti");
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)
console.log(sungokong.yell())

console.log("-----------------------------------------------------")
class Frog extends Animal {
  constructor(nama) {
    super(nama);
  }
  jump() {
    return "hop hop";
  }
}

var kodok = new Frog("keroro");
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)
console.log(kodok.jump())

console.log("=========================No.2=========================")
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}


let clock = new Clock({template: 'h:m:s'});
clock.start();
